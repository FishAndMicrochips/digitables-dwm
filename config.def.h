/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx = 2; /* border pixel of windows */
static const unsigned int snap = 32;    /* snap pixel */
static const unsigned int systraypinning =
    0; /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor
          X */
static const unsigned int systrayspacing = 2; /* systray spacing */

/* 1: if pinning fails, display systray on the first monitor, 
 * False: display systray on the last monitor */
static const int systraypinningfailfirst = 1;
static const int showsystray = 1;     /* 0 means no systray */
static const unsigned int gappih = 0; /* horiz inner gap between windows */
static const unsigned int gappiv = 0; /* vert inner gap between windows */
static const unsigned int gappoh = 0; /* horiz outer gap between windows and screen edge */
static const unsigned int gappov = 0; /* 1 means no outer gap when there is only one window */
static const int smartgaps = 1; /* vert outer gap between windows and screen edge */
static const int showbar = 1; /* 0 means no bar */
static const int topbar = 1;  /* 0 means bottom bar */

static const char *fonts[] = {
    //"-*-terminus-medium-r-normal-*-14-*-*-*-*-*-*-*"
    "Iosevka Nerd Font:style=regular:size=9",
    // "FontAwesome:size=9:antialiasing=true"
};
// static const char dmenufont[]       = "Iosevka Nerd
// Font:style=regular:pixelsize=11";
static const char col_bkg[] =  "#1d2021";
static const char col_highlight[] = "#427b58" // "#689d6a";
;
static const char col_frg[] = "#ebdbb2";
static const char col_frg_bright[] = "#fbf1c7";
static const char *colors[][3] = {
    /*               fg         	bg         	border   */
    [SchemeNorm] = {col_frg,        col_bkg,        col_bkg},
    [SchemeSel]  = {col_frg_bright, col_highlight,  col_highlight},
    //[SchemeSel]  = {col_bkg,        col_highlight,  col_highlight},
};

/* tagging */
static const char *tags[] = { " ", " ", " ", " ", " "};

static const Rule rules[] =
    {
        /* xprop(1):
         *	WM_CLASS(STRING) = instance, class
         *	WM_NAME(STRING) = title
         */
        /* class              instance    title       tags mask     isfloating
           monitor */
        //{ "firefox",          NULL,       NULL,       1 << 1,       0, -1 },
        //{ "discord",          NULL,       NULL,       1 << 2,       0, -1 },
        //{ "riot-desktop",     NULL,       NULL,       1 << 2,       0, -1 },
        //{"TelegramDesktop", NULL, NULL, 1 << 2, -1, -1},
        //{"discord"        , NULL, NULL, 1 << 2, -1, -1},
        //{"element-desktop", NULL, NULL, 1 << 2, -1, -1},
        {"Evolution"      , NULL, NULL, 1 << 1, -1, -1},
        //{ "steam",            NULL,       NULL,       1 << 4,       0, -1 },
        //{ "emacs",            NULL,       NULL,       1 << 5,       0, -1 },
        //{ "krita",            NULL,       NULL,       1 << 6,       0, -1 },
        //{ "thunderbird",      NULL,       NULL,       1 << 7,       0, -1 },
        //{ "libreoffice",      NULL,       NULL,       1 << 8,       0, -1 }
        //{}
};

/* layout(s) */
static const float mfact = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster = 1;    /* number of clients in master area */
static const int resizehints = 0; /* 1 means respect size hints in tiled resizals */
static int attachbelow = 1; /* 1 means attach after the currently active window */

#include "layouts.c"
static const Layout layouts[] = {
    /* symbol     arrange function */
    {"| ﬿  ", tile}, /* first entry is default */
    {"|   ", NULL}, /* no layout function means floating behavior */
    {"| [1]", monocle},
    {"|   ", bstack},
    {"|   ", bstackhoriz},
    {"|   ", gaplessgrid},
    {"| 礪  ", centeredfloatingmaster},
    {"|   ", centeredmaster},
    {NULL, NULL},
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY, TAG)                                                      \
  {MODKEY, KEY, view, {.ui = 1 << TAG}},                                       \
      {MODKEY | ControlMask, KEY, toggleview, {.ui = 1 << TAG}},               \
      {MODKEY | ShiftMask, KEY, tag, {.ui = 1 << TAG}},                        \
      {MODKEY | ControlMask | ShiftMask, KEY, toggletag, {.ui = 1 << TAG}},

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd)                                                             \
  {                                                                            \
    .v = (const char *[]) { "/bin/sh", "-c", cmd, NULL }                       \
  }

#include "mpdcontrol.c"

/* commands */
static char dmenumon[2] =
    "0"; /* component of dmenucmd, manipulated in spawn() */

#define TERMINAL_NAME "st"

static const char *dmenucmd[] = {"dmenu_run", NULL};
static const char *termcmd[] = {TERMINAL_NAME, NULL};
static const char *passcmd[] = {"passmenu", NULL};
static const char *screenshotcmd[] = {"xfce4-screenshooter", NULL};
static const char *lockcmd[] = {"xflock4", NULL};
static const char *quitcmd[] = {"pkill", "dwm", NULL};
static const char *editorcmd[] = {"emacsclient", "-c", NULL};
static const char *filemanagercmd[] = {"urxvt", "-e", "ranger", NULL};

/* static const char *termcmd[]  = { "st", NULL }; */
/* static const char *tabtermcmd[]  = { "tst", NULL }; */
/* static const char *browcmd[] = { "surf -p", NULL }; */
/* static const char *tabbrowcmd[] = { "tsurf", NULL }; */

static Key keys[]                                      = {
    /* modifier                     key        function        argument */
    // Custom commands
    {MODKEY, XK_x, spawn, {.v                          = dmenucmd}},
    {MODKEY | ShiftMask, XK_x, spawn, {.v              = termcmd}},
    {MODKEY, XK_q, spawn, {.v                          = lockcmd}},
    {MODKEY, XK_s, spawn, {.v                          = screenshotcmd}},
    {MODKEY, XK_e, spawn, {.v                          = filemanagercmd}},
    {MODKEY, XK_p, spawn, {.v                          = passcmd}},
    {MODKEY | ShiftMask, XK_e, spawn, {.v              = editorcmd}},
    {MODKEY, XK_b, togglebar, {0}},
    {MODKEY, XK_j, focusstack, {.i                     = +1}},
    {MODKEY, XK_k, focusstack, {.i                     = -1}},
    {MODKEY, XK_i, incnmaster, {.i                     = +1}},
    {MODKEY, XK_d, incnmaster, {.i                     = -1}},
    {MODKEY, XK_h, setmfact, {.f                       = -0.05}},
    {MODKEY, XK_l, setmfact, {.f                       = +0.05}},
    {MODKEY, XK_Return, zoom, {0}},
    {MODKEY, XK_Tab, view, {0}},
    {MODKEY | ShiftMask, XK_c, killclient, {0}},
    // Layout bindings
    {MODKEY, XK_t, setlayout, {.v                      = &layouts[0]}},
    {MODKEY, XK_f, setlayout, {.v                      = &layouts[1]}},
    {MODKEY, XK_m, setlayout, {.v                      = &layouts[2]}},
    {MODKEY, XK_u, setlayout, {.v                      = &layouts[3]}},
    {MODKEY, XK_o, setlayout, {.v                      = &layouts[4]}},
    {MODKEY, XK_g, setlayout, {.v                      = &layouts[5]}},
    {MODKEY, XK_v, setlayout, {.v                      = &layouts[6]}},
    {MODKEY | ShiftMask, XK_v, setlayout, {.v          = &layouts[7]}},
    {MODKEY | ControlMask, XK_comma, cyclelayout, {.i  = -1}},
    {MODKEY | ControlMask, XK_period, cyclelayout, {.i = +1}},
    {MODKEY, XK_space, setlayout, {0}},
    {MODKEY | ShiftMask, XK_space, togglefloating, {0}},
    {MODKEY, XK_0, view, {.ui                          = ~0}},
    {MODKEY | ShiftMask, XK_0, tag, {.ui               = ~0}},
    {MODKEY, XK_comma, focusmon, {.i                   = -1}},
    {MODKEY, XK_period, focusmon, {.i                  = +1}},
    {MODKEY | ShiftMask, XK_comma, tagmon, {.i         = -1}},
    {MODKEY | ShiftMask, XK_period, tagmon, {.i        = +1}},
    TAGKEYS(XK_1, 0) 
    TAGKEYS(XK_2, 1) 
    TAGKEYS(XK_3, 2) 
    TAGKEYS(XK_4, 3)
    TAGKEYS(XK_5, 4) 
    TAGKEYS(XK_6, 5) 
    TAGKEYS(XK_7, 6) 
    TAGKEYS(XK_8, 7)
    TAGKEYS(XK_9, 8)
    {MODKEY | ShiftMask, XK_q, quit, {0}},
    {MODKEY|ControlMask, XK_q, quit, {1} }, 
    {MODKEY, XK_F1, mpdchange, {.i                     = -1}},
    {MODKEY, XK_F2, mpdchange, {.i                     = +1}},
    {MODKEY, XK_Escape, mpdcontrol, {0}},
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle,
 * ClkClientWin, or ClkRootWin */
static Button buttons[] = {
    /* click                event mask      button          function argument
     */
    {ClkLtSymbol, 0, Button1, setlayout, {0}},
    {ClkLtSymbol, 0, Button3, setlayout, {.v = &layouts[2]}},
    {ClkWinTitle, 0, Button2, zoom, {0}},
    {ClkStatusText, 0, Button2, spawn, {.v = termcmd}},
    {ClkClientWin, MODKEY, Button1, movemouse, {0}},
    {ClkClientWin, MODKEY, Button2, togglefloating, {0}},
    {ClkClientWin, MODKEY, Button3, resizemouse, {0}},
    {ClkTagBar, 0, Button1, view, {0}},
    {ClkTagBar, 0, Button3, toggleview, {0}},
    {ClkTagBar, MODKEY, Button1, tag, {0}},
    {ClkTagBar, MODKEY, Button3, toggletag, {0}},
};
