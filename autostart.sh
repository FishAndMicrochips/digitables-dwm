#!/bin/bash
slstatus &
redshift &
xfce4-power-manager &
xfce4-clipman &
nm-applet &
/usr/lib/mate-polkit/polkit-mate-authentication-agent-1 2>&1 > /dev/null &
xrandr --output HDMI-1 --auto --right-of DP-0 &
mpd ~/.config/mpd/mpd.conf
mpc update &
btpd &
feh --bg-fill Pictures/backgrounds/lighthouse.jpg
